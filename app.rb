require 'sinatra'
require 'sequel'
require 'logger'
require './engine/board'
require './helpers'
require 'psych'

if ENV["DATABASE_URL"]
  db_address = ENV["DATABASE_URL"]
else
  config = Psych.load_file("./config.yml")
  db_config = config['database']
  if db_config['db_username'] or db_config['db_password']
    login = "#{db_config['db_username']}:#{db_config['db_password']}@"
  else
    login = ''
  end
  db_address = "postgres://#{login}#{db_config['db_address']}/#{db_config['db_name']}"
end


DB = Sequel.connect db_address
require './models/init'


include Helpers
enable :sessions
logger = Logger.new $stdout
logger.level = Logger::INFO
configure :development do
  set :show_exceptions, true
  logger = Logger.new $stdout
  logger.level = Logger::DEBUG
end

get '/' do
  erb :index
end

get '/create_game/?' do
  board = Board.new
  g = Game.create(
    name: "",
    board: Marshal.dump(board)
  )
  g.name = g.id.to_s
  g.save
  redirect "/game/#{g.id}"
end

get '/create_sns_game/?' do
  board = Board.new
  names = ['scott', 'sacks'].shuffle
  board.add_player names[0], 1
  board.add_player names[1], 1
  board.start
  g = Game.create(
    name: "",
    board: Marshal.dump(board)
  )
  g.name = g.id.to_s
  g.save

  redirect "/game/#{g.id}"
end

get '/games/?' do
  @games = Game.order_by(:id).all
  erb :games
end

get '/game/:game_id/start/?' do
  logger.info "starting game #{params['game_id']}"
  @game = Game[params['game_id'].to_i]
  @board = @game.get_board
  @board.start
  @game.board = Marshal.dump @board
  @game.save
  redirect "/game/#{params['game_id']}"
end

get '/game/:game_id/?' do
  @game = Game[params['game_id'].to_i]
  @board = Marshal.load @game.board
  if @board.started
    erb :game
  else
    erb :pregame
  end
end

post '/game/:game_id/addplayer' do
  logger.info "adding player #{params["playername"]}"
  player = params["playername"]
  @game = Game[params['game_id'].to_i]
  @board = Marshal.load(@game.board)
  begin
    @board.add_player player, 1
  rescue => e
    @error_message = e.message
  end
  @game.board = Marshal.dump @board
  @game.save
  redirect "/game/" + params["game_id"]
end
