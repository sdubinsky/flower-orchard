# config valid for current version and patch releases of Capistrano
set :application, "flower-orchard"
set :repo_url, "git@gitlab.com:sdubinsky/flower-orchard.git"
set :deploy_to, "/flower-orchard"

set :linked_files, fetch(:linked_files, []).push('config.yml')
set :linked_files, fetch(:linked_files, []).push('session_secret.txt')
set :linked_dirs, fetch(:linked_dirs, []).push('log')

set :branch, 'master'
set :default_env, {
      'BUNDLE_PATH': '/flower-orchard/.bundle',
      'ASDF_DATA_DIR': '/opt/asdf'
    }


desc 'migrations'
task 'db:migrate'.to_sym do
  on roles(:web) do
    within release_path do
      execute 'asdf', 'exec', 'bundle', 'exec', 'rake', 'db:migrate'
    end
  end
end

desc 'bundle'
task :bundle do
  on roles(:web) do
    within release_path do
      execute 'asdf', 'exec', 'bundle'
    end
  end
end

namespace :systemd do
  desc 'restart server'
  task :restart do
    on roles(:web) do
      sudo :systemctl, 'restart', 'flower-orchard'
    end
  end
end

after 'deploy', 'bundle'
after 'deploy', 'db:migrate'
after 'deploy', 'systemd:restart'
