class Card
  attr_accessor :name, :description, :cost, :active_numbers, :count, :value, :color, :symbol, :search_dict, :explanation
  def initialize(name)
    base_card = Card.card_list[name]
    @name = name
    @description = base_card[0]
    @cost = base_card[1]
    @active_numbers = base_card[2]
    @value = base_card[3]
    @search_dict = base_card[4]
    @color = base_card[5]
    @symbol = base_card[6]
    @explanation = base_card[7]
    @count = 1
  end

  def self.get_cards card_list
    card_list.keys.map{|c| Card.new c}
  end

  def symbol_img
    case @symbol
    when :crop
      "🪴"
    when :fruit
      "🍎"
    when :cup
      "☕"
    when :tower
      "🗼"
    when :gear
      "⚙"
    when :store
      "🏠"
    when :cow
      "🐄"
    when :boat
      "⛵"
    when :factory
      "🏭"
    end

  end

  def == other
    name.to_sym == other.name.to_sym
  end

  def to_s
    "#{count} #{name} $#{cost}.  #{active_numbers}"
  end

  def to_h
    {
      name: name,
      cost: cost,
      description: to_s,
      active_numbers: active_numbers,
      count: count,
      color: color,
      symbol_img: symbol_img,
      symbol: symbol,
      explanation: explanation
    }
  end

  def self.card_list
    self.regular_card_list.merge self.purple_card_list
  end

  def self.regular_card_list
    # description, cost, active numbers, value, search_dict, color, symbol
    {
      ranch: ['ranch', 1, [2], 1, {}, :blue, :cow, "Get 1 coin from the bank, on anyone’s turn."],
      cheese_factory: ['cheese factory', 5, [7], 3, {symbol: :cow}, :green, :factory, "Get 3 coins from the bank for each 🐄 establishment that you own, on your turn only."],
      flower_shop: ['flower shop', 1, [6], 1, {name: :flower_orchard}, :green, :store, "Get 1 coin from the bank for each flower orchard you own, on your turn only."],
      convenience_store: ['convenience store', 2, [4], 3, {}, :green, :store, "Get 3 coins from the bank, on your turn only."],
      bakery: ["bakery", 1, [2,3], 1, {}, :green, :store, "Get 1 coin from the bank, on your turn only."],
      food_warehouse: ["food warehouse", 2, [12, 13], 2, {symbol: :cup}, :green, :factory, "Get 2 coins from the bank for each ☕ establishment you own, on your turn only."],
      mine: ['mine', 6, [9], 5, {}, :blue, :gear, "Get 5 coins from the bank, on anyone’s turn."],
      pizza_joint: ['pizza joint', 1, [7], 1, {}, :red, :cup, "Get 1 coin from the player who rolled the dice."],
      cafe: ['cafe', 2, [3], 1, {}, :red, :cup, "Get 1 coin from the player who rolled the dice."],
      flower_orchard: ['flower orchard', 2, [4], 1, {}, :blue, :crop, "Get 1 coin from the bank, on anyone’s turn."],
      family_restaurant: ['family restaurant', 3, [9, 10], 2, {}, :red, :cup, "Get 2 coins from the player who rolled the dice."],
      mackerel_boat: ['mackerel boat', 2, [8], 3, {has: :harbor}, :blue, :boat, "If you have a harbor, get 3 coins from the bank on anyone’s turn."],
      hamburger_stand: ['hamburger_stand', 1, [8], 1, {}, :red, :cup, "Get 1 coin from the player who rolled the dice."],
      sushi_bar: ["sushi bar", 4, [1], 3, {has: :harbor}, :red, :cup, "If you have a harbor, you get 3 coins from the player who rolled the dice."],
      wheat_field: ["wheat_field", 1, [1], 1, {}, :blue, :crop, "Get 1 coin from the bank, on anyone’s turn."],
      tuna_boat: ['tuna boat', 7, [12, 13, 14], 0, {has: :harbor}, :blue, :boat, "On anyone’s turn: rolls 2 dice. If you have a harbor, you get as many coins as the dice total."],
      apple_orchard: ['apple orchard', 3, [10], 3, {}, :blue, :crop, "Get 3 coins from the bank, on anyone’s turn."],
      forest: ['forest', 3, [5], 1, {}, :blue, :gear, "Get 1 coin from the bank, on anyone’s turn."],
      furniture_factory: ['furniture_factory', 3, [8], 3, {symbol: :gear}, :green, :factory, "Get 3 coins from the bank for each ⚙ establishment that you own, on your turn only."],
      fruit_market: ['fruit market', 2, [11, 12], 2, {symbol: :crop}, :green, :fruit, "Get 2 coins from the bank for each 🪴 establishment that you own, on your turn only."]
    }
  end

  def self.purple_card_list
    {
      tv_station: ['tv station', 7, [6], 5, {}, :purple, :tower, "Take 5 coins from any one player, on your turn only."],
      business_center: ['business center', 8, [6], 0, {}, :purple, :tower, "Trade one non-tower establishment with another player, on your turn only."],
      publisher: ['publisher', 5, [7], 1, {}, :purple, :tower, "Get 1 coin from each player for each cup and store establishment they have, on your turn only."],
      stadium: ['stadium', 6, [6], 2, {}, :purple, :tower, "Get 2 coins from all players, on your turn only."],
      tax_office: ['tax office', 4, [8, 9], 0, {}, :purple, :tower, "Take half (rounded down) of the coins from each player who has 10 coins or more, on your turn only."]
    }
  end
end
