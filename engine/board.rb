require 'json'
require_relative 'card'
require_relative 'player'
require_relative 'turn'
require 'debug'

class Board
  attr_accessor :players, :deck, :field, :started, :turnHistory, :current_turn, :commands, :game_over, :tv_station, :business_center, :current_player
  def initialize
    @players = []
    @commands = []
    @log = []
    @deck = []
    @field = []
    @started = false
    @turn_history = []
    @current_player = nil
    @game_over = false
    @tv_station = false
    @business_center = false
  end

  def new_deck
    cards = []
    5.times do
      cards += Card.get_cards Card.regular_card_list
    end
    @players.count.times do
      cards += Card.get_cards Card.purple_card_list
    end
    cards.shuffle
  end

  def start
    @started = true
    @deck = new_deck
    deal_field
    @current_player = 0
    @current_turn = Turn.new current_player
  end

  def current_player
    @players[@current_player]
  end

  def add_player name, id
    raise "game has already started" if started
    raise "already at max player count" if players.count >= 4
    @log.append "added player #{name}"
    players << Player.new(name, id)
  end

  def roll_dice dice_count
    @current_turn.roll_dice dice_count
    @log.append "rolled: #{dice_display}"
    if !can_add_two? and !can_roll_again?
      run_turn
    end
  end

  def total
    @current_turn.roll_one + @current_turn.roll_two
  end

  def dice_display
    result = "#{current_turn.roll_one}"
    result += " :: #{current_turn.roll_two}" if current_turn.dice_count == 2
    result
  end

    def buy_card card_name
    raise "please start game" if not @started
    return unless @current_turn.paid_out
    return if @current_turn.bought
    if card_name == 'pass'
      @log.append "#{current_player.name} passed."
      @current_turn.bought = :pass
      return
    end
    card = field.find{|x| x.name.to_sym == card_name.to_sym}
    if card.color == :purple
      if current_player.hand.include? card
        @log.append "#{current_player.name} already has a #{card_name}"
        return
      end
    end
    raise "couldn't find that card" if not card
    current_player.buy_card card_name.to_sym
    @log.append "#{current_player.name} bought #{card_name}"
    replace_in_field card
    @current_turn.bought = card_name.to_sym
  end

  def activate_improvement improvement_name
    raise "please start game" if not @started
    return unless @current_turn.paid_out
    return if @current_turn.bought
    current_player.activate_improvement improvement_name
    @current_turn.bought = improvement_name.to_sym
    @log.append "#{current_player.name} activated #{improvement_name}"
  end

  def run_turn
    return if @current_turn.paid_out
    dice_total = current_turn.roll_one + current_turn.roll_two
    dice_total += 2 if current_turn.added_two
    check_specials dice_total
    @players.each do |p|
      if p != current_player
        fine = p.activate_red_cards dice_total
        charge = current_player.pay fine
        p.cash += charge
        @log.append "#{p.name} gets $#{charge} from #{current_player.name}" if fine > 0
      end
    end
    @players.each do |p|
      if p == current_player
        earnings = p.activate_green_cards dice_total
      else
        earnings = 0
      end

      earnings += p.activate_blue_cards dice_total
      @log.append "#{p.name} earned $#{earnings} this turn" if earnings > 0
    end
    use_tuna_boat dice_total

    use_stadium dice_total
    use_publisher dice_total
    use_tax_office dice_total
    if current_player.cash == 0
      current_player.cash += 1
      @log.append "#{current_player.name} gets their pItY cOiN"
    end
    @current_turn.paid_out = true
  end

  def end_turn
    check_game_over
    if game_over
      @log.append "Game over.  #{current_player.name} won!"
      return
    end

    if tv_station or business_center
      return
    end

    return unless @current_turn.bought

    if @current_turn.bought == :pass and
      current_player.gets_free_money?
      current_player.cash += 10
      @log.append "#{current_player.name} gets 10 from the airport"
    end

    if !(current_turn.roll_one == current_turn.roll_two and
         current_player.gets_another_turn? and
         current_turn.bought.to_sym != :amusement_park)
      @current_player = (@current_player + 1) % @players.length
      @turn_history << @current_turn
      @log.append "#{current_player.name}'s turn"
    else
      @log.append "#{current_player.name} gets another turn"
    end
    @current_turn = Turn.new current_player
  end

  def can_add_two?
    @current_turn.can_add_two? and current_player.can_add_two? and not @current_turn.added_two
  end

  def can_roll_two?
    current_player.can_roll_two? && !@current_turn.paid_out
  end

  def can_roll_again?
    current_turn.rolls == 1 and current_player.can_roll_again? and !@current_turn.paid_out
  end

  def check_specials total
    tax_office = current_player.hand.find{|x| x.name == :tax_office}
    if tax_office and tax_office.active_numbers.include? total
      @tax_office = true
    end

    business_center = current_player.hand.find{|x| x.name == :business_center}
    if business_center and business_center.active_numbers.include? total
      @business_center = true
    end

    tv_station = current_player.hand.find{|x| x.name == :tv_station}
    if tv_station and tv_station.active_numbers.include? total
      @tv_station = true
    end
  end

  def use_tv_station target
    return if not @tv_station
    target = players.find{|a| a.name == target}
    if target == current_player
      @log.append "can't target current player"
      return
    end
    fine = 5
    if target.cash < 5
      fine = target.cash
    end
    @log.append "#{current_player.name} gets $#{fine} from #{target.name} through a tv station"
    target.cash -= fine
    current_player.cash += fine
    @tv_station = false
    end_turn
  end

  def use_business_center my_card, target, their_card
    return if not @business_center
    target_player = players.find{|a| a.name == target}
    my_card = current_player.hand.find{|a| a.name == my_card.to_sym}
    their_card = target_player.hand.find{|a| a.name == their_card.to_sym}
    return if their_card.color == :purple
    return if my_card.color == :purple
    if my_card.count == 1
      current_player.hand.delete(my_card)
    else
      my_card.count -= 1
    end
    if their_card.count == 1
      target_player.hand.delete(their_card)
    else
      their_card.count -= 1
    end
    current_player.add_card Card.new(their_card.name)
    target_player.add_card Card.new(my_card.name)
    @business_center = false
    @log.append "#{current_player.name} traded their #{my_card.name} for #{target}'s #{their_card.name}"
    end_turn
  end

  def use_stadium total
    card = current_player.hand.find{|c| c.name == :stadium}
    return unless card
    return unless card.active_numbers.include? total
    players.each do |player|
      next if player == current_player
      fine = 2
      fine = player.cash if player.cash < fine
      player.cash -= fine
      current_player.cash += fine
      @log.append "#{current_player.name} gets $#{fine} from #{player.name} from stadium"
    end
  end

  def use_publisher total
    card = current_player.hand.find{|c| c.name == :publisher}
    return unless card
    return unless card.active_numbers.include? total
    players.each do |player|
      next if player == current_player
      fine = player.hand.select{|a| [:cup, :store].include? a.symbol}.
               map{|a| a.count}.
               reduce(0, :+)
      fine = player.cash if player.cash < fine
      player.cash -= fine
      current_player.cash += fine
      @log.append "#{current_player.name} gets $#{fine} from #{player.name} from publisher"
    end
  end

  def use_tax_office total
    card = current_player.hand.find{|c| c.name == :tax_office}
    return unless card
    return unless card.active_numbers.include? total
    players.each do |player|
      next if player == current_player
      if player.cash >= 10
        fine = player.cash / 2
        player.cash -= fine
        current_player.cash += fine
        @log.append "#{current_player.name} gets $#{fine} from #{player.name} from taxes"
      end
    end
  end

  def use_harbor
    return unless current_turn.can_add_two?
    @log.append "Harbor used.  New total: #{@current_turn.roll_one + @current_turn.roll_two + 2}"
    current_turn.added_two = true
    run_turn
  end

  def use_tuna_boat total
    return unless total >= 12
    roll = rand(1..6) + rand(1..6)
    @log.append("tuna boat roll: #{roll}")
    @players.each do |player|
      next unless player.has_harbor?
      boats = player.hand.find{|c| c.name == :tuna_boat}
      next unless boats
      boat_count = boats.count
      player.cash += roll * boat_count
      if boat_count == 2
        @log.append("#{player.name} gets #{roll * boat_count} from their twona boat")
      else
        @log.append("#{player.name} gets #{roll * boat_count} from their tuna boat")
      end
    end
  end

  def check_game_over
    @game_over = current_player.has_won?
  end

  def replace_in_field card
    field_card = field.find{|x| x == card}
    field_card.count -= 1
    if field_card.count == 0
      field.delete(field_card)
      deal_field
    end
  end

  def deal_field
    while field.size < 10
      card = deck.pop
      if field.include? card
        field.find{|x| x == card}.count += 1
      else
        field << card
      end
    end
  end

  def add_message message
    return if message.length == 0
    @log.append "message: #{message}"
  end

  def to_s
    "Players: #{players.map{|s| s.to_s}}<br />Field: #{field.map{|f| f.to_s}}"
  end

  def to_h
    {
      current_player: current_player.to_h,
      players: players.map{|a| a.to_h},
      field: field.map{|f| f.to_h},
      current_turn: current_turn.to_h,
      can_roll_two: can_roll_two?,
      can_roll_again: can_roll_again?,
      can_add_two: can_add_two?,
      log: @log.last(20).reverse,
      business_center: @business_center,
      tv_station: @tv_station,
      game_over: @game_over
    }
  end

  def to_json
    to_h.to_json
  end
end
