class Improvement
  attr_accessor :name, :description, :cost, :active, :explanation
  def initialize(name, description, cost, explanation)
    @name = name
    @description = description
    @cost = cost
    @active = false
    @explanation = explanation
  end

  def == other
    name.to_sym == other.to_sym
  end

  def to_h
    {
      name: name,
      cost: cost,
      active: active,
      explanation: explanation
    }
  end
end
