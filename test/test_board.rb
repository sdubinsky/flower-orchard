require 'minitest/autorun'
require 'rack/test'
require_relative '../engine/board'

class TestBoard < Minitest::Test

  def setup
    @board = Board.new
    @board.add_player 'scott', 1
    @board.add_player 'sacks', 2
  end

  def test_create_board
    assert_equal @board.players.count, 2
  end

  def test_start_game
    @board.start
  end

  def test_field
    @board.start
    assert_equal 10, @board.field.length

  end

  def test_red_cards
    @board.start
    @board.players[1].hand << Card.new(:sushi_bar)
    @board.players[1].activate_improvement(:harbor)
    @board.current_player = 0
    @board.current_turn.roll_one = 1
    @board.current_turn.roll_two = 0
    @board.run_turn
    assert_equal 1, @board.current_player.cash
  end

  def test_flower_orchard
    @board.start
    @board.current_player.hand << Card.new(:flower_orchard)
    @board.current_turn.roll_one = 4
    @board.current_turn.roll_two = 0
    @board.run_turn
    assert_equal 4, @board.current_player.cash
  end

  def test_purple_cards_exist
    @board.start
    assert @board.deck.find{|a| a.name == :tax_office}
  end

  def test_use_publisher
    @board.start
    @board.current_player.hand << Card.new(:publisher)
    @board.players[1].hand << Card.new(:bakery)
    @board.current_turn.roll_one = 4
    @board.current_turn.roll_two = 3
    @board.run_turn
    assert_equal 5, @board.current_player.cash
  end

  def test_use_business_center
    @board.start
    @board.current_player.hand << Card.new(:business_center)
    @board.business_center = true
    @board.current_player.hand << Card.new(:wheat_field)
    @board.players[1].hand << Card.new(:bakery)
    @board.use_business_center(:wheat_field, @board.players[1].name, :bakery)
    assert @board.players[0].hand.include? Card.new(:bakery)
    assert !(@board.players[0].hand.include? Card.new(:wheat_field))

    assert @board.players[1].hand.include? Card.new(:wheat_field)
    assert !(@board.players[1].hand.include? Card.new(:bakery))
  end

  def test_harbor
    @board.start
    @board.current_player.improvements[1].active = true
    @board.current_player.improvements[2].active = true
    @board.current_player.hand << Card.new(:wheat_field)
    @board.current_player.hand << Card.new(:fruit_market)
    @board.current_player.cash = 0
    assert @board.can_roll_two?
    @board.current_turn.roll_one = 8
    @board.current_turn.roll_two = 3
    @board.current_turn.rolls = 1
    assert @board.can_add_two?
    @board.use_harbor
    assert @board.current_turn.added_two
    @board.run_turn
    assert @board.current_player.cash = 2
  end

  def test_buys_multiple_purples
    @board.start
    @board.current_player.cash = 10
    @board.field << Card.new(:stadium)
    @board.current_player.hand << Card.new(:stadium)
    @board.current_turn.paid_out = true
    @board.buy_card :stadium
    assert_equal @board.current_turn.bought, false
  end

  def test_airport
    @board.start
    @board.current_turn.roll_one = 8
    @board.current_turn.roll_two = 3
    player = @board.current_player
    player.cash = 30
    @board.current_player.activate_improvement :airport
    @board.run_turn

    @board.buy_card 'pass'
    @board.end_turn
    assert_equal 11, player.cash
  end

  def test_mackerel_boat
    @board.start
    @board.current_turn.roll_one = 5
    @board.current_turn.roll_two = 3
    player = @board.current_player
    @board.current_player.hand << Card.new(:mackerel_boat)
    @board.run_turn
    assert_equal 3, player.cash
  end


  def test_amusement_park_not_the_turn_you_buy_it
    @board.start
    @board.current_turn.roll_one = 8
    @board.current_turn.roll_two = 8
    @board.run_turn
    @board.current_player.cash = 40
    @board.activate_improvement 'amusement_park'
    player = @board.current_player
    @board.end_turn
    refute_equal player.name, @board.current_player.name
  end
end
