require 'minitest/autorun'
require 'rack/test'
require_relative '../engine/board'

class TestCards < Minitest::Test
  def test_family_restaurant_color
    c = Card.new(:family_restaurant)
    assert_equal :red, c.color
  end

  def test_all_cards_color
    cards = Card.get_cards Card.card_list
    cards.map{|c| assert [:red, :blue, :green, :purple].include? c.color}
  end
end
