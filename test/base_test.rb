require 'minitest/autorun'
require 'rack/test'
require 'sequel'
require 'sqlite3'
require 'logger'
require 'debug'

class BaseTest < Minitest::Test
  DB = Sequel.sqlite
end
