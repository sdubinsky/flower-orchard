if (window.document.location.hostname === "localhost") {
  var scheme = "ws://";
}else{
  var scheme = "wss://";
}
var uri = scheme + window.document.location.host + window.document.location.pathname + "/";
var ws = new WebSocket(uri);

var getGameId = function(){
  var game_id = window.document.location.pathname.split("/");
  game_id = game_id[game_id.length - 1];
  return game_id;
};

var buildPlayerElem = function(player, can_buy){
  let player_elem = document.createElement("div");
  player_elem.className += "player-div";
  let name_elem = document.createElement("div");
  name_elem.className += "player-name";
  name_elem.innerHTML =  player.name + " $" + player.cash;
  player_elem.appendChild(name_elem);
  player_elem.innerHTML += "Cards:<br />";
  player.hand.forEach(function(card){
    let card_elem = document.createElement("div");
    card_elem.className += "player-card";
    card_elem.className += " " + card.color;
    card_elem.innerHTML = card.symbol_img + " " + card.count + " " + card.name + " (" + card.active_numbers + ")";
    let expl = document.createElement("span");
    expl.className += "explanation";
    expl.innerHTML = "?";
    expl.setAttribute("title", card.explanation);
    card_elem.appendChild(expl);
    player_elem.appendChild(card_elem);
  });
  player_elem.innerHTML += "Improvements<br />";
  player.improvements.forEach(function(improvement) {
    let imp_elem = document.createElement("div");
    imp_elem.className += "player-improvement " + "imp-active-" + improvement.active;
    imp_elem.innerHTML = "name: " + improvement.name + " $" + improvement.cost;
    player_elem.appendChild(imp_elem);
    let expl = document.createElement("span");
    expl.className += "explanation";
    expl.innerHTML = "?";
    expl.setAttribute("title", improvement.explanation);
    imp_elem.appendChild(expl);
    if (can_buy && !improvement.active && !board.game_over){
      buy = document.createElement("button");
      buy.className = "buy-button";
      buy.onclick = function (event) {
        ws.send(JSON.stringify({'game_id': getGameId(), 'message': 'buy improvement ' + improvement.name}));

      };
      buy.innerHTML = "Activate";
      imp_elem.appendChild(buy);
    }
  });
  return player_elem;
};

var displayBoard = function(board) {
  console.log(board);
  document.title = "Machi Koro (" + board.current_player.name + "'s turn)";
  //players
  var players_div = document.querySelector('#players');
  players_div.innerHTML = "";
  board.players.forEach(function(player){
    let can_buy = (board.current_turn.paid_out) && (player.name == board.current_player.name);
    let elem = buildPlayerElem(player, can_buy);
    players_div.appendChild(elem);
  });

  //field
  var field_div = document.querySelector("#field");
  field_div.innerHTML = "";
  board.field.forEach(function(card){
    let elem = document.createElement("div");
    elem.className += card.color;
    elem.innerHTML = card.symbol_img + " " + card.description;
    field_div.appendChild(elem);
    if (board.current_turn.paid_out && !board.game_over){
      let buyme = document.createElement("button");
      buyme.innerHTML = "Buy";
      buyme.className += "buy-button";
      elem.onclick = function (event){
        ws.send(JSON.stringify({'game_id': getGameId(), 'message': "buy card " + card.name}));
      };
      elem.appendChild(buyme);
    }
    let expl = document.createElement("span");
    expl.className += "explanation";
    expl.innerHTML = "?";
    expl.setAttribute("title", card.explanation);
    elem.appendChild(expl);

  });

  //special cards
  if (board.tv_station && !board.game_over){
    document.querySelector("#tv-station-form").style.display = '';
    let button = document.querySelector("#tv-station-submit");
    button.onclick = function (event) {
      let target = document.querySelector("#tv-station-target").value;
      ws.send(JSON.stringify({'game_id': getGameId(), 'message': "use tv_station " + target}));
    };
  } else {
    let button = document.querySelector("#tv-station-submit");
    document.querySelector("#tv-station-target").value = '';
    button.onclick = function (event) {};
    document.querySelector("#tv-station-form").style.display = 'none';
  }

  if (board.business_center && !board.game_over) {
    document.querySelector("#business-center-form").style.display = '';
    var select = document.getElementById("business-center-own-card");
    board.current_player.hand.forEach(function(card){
      if (card.color === "purple"){
        return;
      }
      var elem = document.createElement("option");
      elem.id = card.name;
      elem.name = card.name;
      elem.text = card.name;
      select.appendChild(elem);
    });

      var select = document.getElementById("business-center-target-player");
    board.players.forEach(function(player){
      if (player.name === board.current_player.name){
        return;
      }
      var elem = document.createElement("option");
      elem.id = player.name;
      elem.name = player.name;
      elem.text = player.name;
      select.appendChild(elem);
    });

    let button = document.querySelector("#business-center-submit");
    button.onclick = function (event) {
      let user_card = document.querySelector("#business-center-own-card").value;
      let target_player = document.querySelector("#business-center-target-player").value;
      let target_card = document.querySelector("#business-center-target-card").value;
      ws.send(JSON.stringify({'game_id': getGameId(), 'message': "use business_center " + user_card + " " + target_player + " " + target_card}));
    };
  }
  if (!board.business_center){
    let button = document.querySelector("#business-center-submit");
    button.onclick = function (event) {};
      document.querySelector("#business-center-form").style.display = 'none';

      var select = document.getElementById("business-center-target-player");
      select.innerHTML = '';
  }
  //pass button
  if (board.current_turn.rolls > 0 && !board.game_over){
    pass = document.createElement("button");
    pass.innerHTML = "pass";
    pass.className += "pass";
    pass.onclick = function (event) {
      ws.send(JSON.stringify({'game_id': getGameId(), 'message': 'buy card pass'}));
    };
    field_div.appendChild(pass);
  }
  var pay_out = document.querySelector("#pay-out");
  var harbor = document.querySelector("#harbor");
  pay_out.innerHTML = "";
  harbor.innerHTML = "";

  //log
  let log_div = document.querySelector("#log");
  log_div.innerHTML = "";
  board.log.forEach(function(line){
    let elem = document.createElement("div");
    elem.innerHTML = line;
    log_div.appendChild(elem);
  });

  document.querySelector('#current-player').innerHTML = "current player: " + board.current_player.name + " $" + board.current_player.cash;
  if (board.current_turn.rolls == 0){
    var dice_one = document.querySelector("#diceone");
    dice_one.innerHTML = "";
    var dice_two = document.querySelector("#dicetwo");
    dice_two.innerHTML = "";
  }
  if (board.current_turn.rolls == 0 || board.can_roll_again) {
    var roll_one = document.createElement("button");
    roll_one.className = "roll-button";
    roll_one.onclick = function (event) {
      ws.send(JSON.stringify({'game_id': getGameId(), 'message': 'roll one'}));
    };
    roll_one.innerHTML = "Roll One";
    var roll_two = document.createElement("button");
    roll_two.className = "roll-button";
    roll_two.onclick = function (event) {
      ws.send(JSON.stringify({'game_id': getGameId(), 'message': 'roll two'}));
    };
    roll_two.innerHTML = "Roll Two";
    var rolldice = document.querySelector("#rolldice");
    rolldice.innerHTML = "";
    rolldice.appendChild(roll_one);
    if (board.can_roll_two){
      rolldice.appendChild(roll_two);
    }
  }
  if (board.current_turn.rolls > 0){
    var dice_one = document.querySelector("#diceone");
    dice_one.innerHTML = "First die: " + board.current_turn.roll_one;
  }
  if (board.current_turn.rolls > 0 && !board.can_roll_again){
    var rolldice = document.querySelector('#rolldice');
    rolldice.innerHTML = "";
  }
  if (board.current_turn.rolls > 0 && board.current_turn.dice_count > 1){
    var dice_two = document.querySelector("#dicetwo");
    dice_two.innerHTML = "Second die: " + board.current_turn.roll_two;
  }

  if (board.current_turn.rolls > 0 && !board.current_turn.paid_out) {
    var pay_out = document.querySelector("#pay-out");
    var harbor = document.querySelector("#harbor");
    pay_out.innerHTML = "Accept Roll";
    pay_out.className = "roll-button";
    pay_out.onclick = function (event) {
      ws.send(JSON.stringify({"game_id": getGameId(), 'message': 'run'}));
    };
  }
  if (board.current_turn.rolls > 0 && !board.current_turn.paid_out && board.can_add_two){
    harbor.innerHTML = "Add Two";
    harbor.className = "roll-button";
    harbor.onclick = function (event) {
      ws.send(JSON.stringify({"game_id": getGameId(), 'message': 'use harbor'}));

    }
  }
};

var message = document.querySelector("#message-send");
message.onclick = function (event) {
  var msg = document.getElementById("message-input");
  ws.send(JSON.stringify({"game_id": getGameId(), "message": "send " + msg.value}));
  msg.value = '';
};

document.getElementById("message-input").addEventListener("change", function (event) {
  ws.send(JSON.stringify({"game_id": getGameId(), "message": "send " + event.target.value}));
  event.target.value = '';
});

ws.onmessage = function(message) {
  var board = JSON.parse(message.data);
  if (getGameId() === board.game_id){
    displayBoard(board.board);
  }
};
